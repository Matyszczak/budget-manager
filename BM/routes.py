from flask import render_template, url_for, flash, redirect
from BM import app
from BM.users import RegistrationForm, LoginForm
from BM.models import User, Post


posts = [
    {
        'author': 'Gosia Morawska',
        'title': 'Pensja 1',
        'amount': '3300',
        'date_posted': 'September 20, 2019'
    },
    {
        'author': 'Krzysztof Matyszczak',
        'title': 'Pensja 2',
        'amount': '3100',
        'date_posted': 'September 21, 2019'
    }
]

@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.username.data == 'admin' and form.password.data == 'password':
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)